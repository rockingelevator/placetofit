import django_filters
from django.db.models import Q
from jobs.models import Job, Tag, JobType
from .serializers import JobListSerializer, TagListSerializer, JobTypeListSerializer
from rest_framework.response import Response
from rest_framework import generics
from rest_framework import filters
from rest_framework import serializers

class JobFilter(django_filters.FilterSet):
	max_salary = django_filters.NumberFilter(name="salary", lookup_type='lt')
	min_salary = django_filters.NumberFilter(name="salary", lookup_type='gte')
	job_type = django_filters.CharFilter(name="job_type__job_type")

	class Meta:
		model = Job
		fields = ['min_salary', 'max_salary', 'job_type', 'city']

class JobListView(generics.ListAPIView):
	"""
	List of all jobs
	"""
	queryset = Job.objects.all()
	serializer_class = JobListSerializer
	filter_class = JobFilter

	def get_queryset(self):
		"""
		Additionaly to generic filters 
		it chains querisets respectively for each tag
		"""
		queryset = Job.objects.all()
		tags = self.request.query_params.getlist('tags', None)
		if tags is not None:
			for tag in tags:
				queryset = queryset.filter(tags=tag)
		return queryset

class TagListView(generics.ListAPIView):
	"""
	List of tags for job search 
	"""
	queryset = Tag.objects.all()
	serializer_class = TagListSerializer
	filter_backends = (filters.SearchFilter,)
	search_fields = ('title',)
	filter_fields = ('title', 'tag_type',)

class JobTypeListView(generics.ListAPIView):
	"""
	List of predefined job types
	"""
	queryset = JobType.objects.all()
	serializer_class = JobTypeListSerializer
	filter_fields = ('id', 'job_type',)