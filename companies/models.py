from django.db import models
from django.contrib.auth.models import User

class City(models.Model):
	country = models.CharField(max_length=50, null=True)
	city = models.CharField(max_length=50, null=True)

	class Meta():
		verbose_name_plural = 'cities'

	def __str__(self):
		return "%s, %s" % (self.city, self.country)

class Company(models.Model):
	owner = models.ForeignKey(User)
	name = models.CharField(max_length=50, null=True, blank=True)
	city = models.ForeignKey(City, null=True, blank=True)
	address = models.CharField(max_length=150, null=True, blank=True)
	phone = models.CharField(max_length=20, null=True, blank=True)

	class Meta():
		verbose_name_plural = 'companies'

	def __str__(self):
		return self.name