angular.module('jobs.services', ['ngResource'])
	
	//gets cities from server
	.factory('CityService', ['$resource', function($resource){
		return $resource('/api/companies/cities', null, {
			query: {
				method: 'GET',
				isArray: true
			},
			get: {
				method: 'GET',
				isArray: true
			}
		});
	}])

	//stores cities list in service
	.factory('CityList', function(){
		var defaultCity = {
			'id': undefined,
			'city': 'All cities',
			'country': 'All'
		};
		var cityList = [defaultCity];
		return {
			get: function(){ return cityList; },
			set: function(list){ 
				cityList = list;
				cityList.unshift(defaultCity); 
				return cityList; 
			}
		};
	})

	.factory('TagService', ['$resource', function($resource){
		return $resource('/api/jobs/tags', null, {
			get: {
				method: 'GET',
				isArray: true
			}
		});
	}])

	.factory('FilterList', function(){
		var filters = {};
		return {
			setFilters: function(fl){
				filters = jQuery.extend(true, {}, fl);
				return filters;
			},
			setFilter: function(key, value) {
				filters[key] = value;
				return;
			},
			getFilters: function(){ return filters; },
			isChanged: function(fl){
				if (JSON.stringify(filters) === JSON.stringify(fl)){
					return false;
				}
				return true;
			}
		};
	})

	.factory('JobService', ['$resource', function($resource){
		return $resource('/api/jobs', null, {
			get: {
				method: 'GET',
				isArray: true
			}
		});
	}]);
