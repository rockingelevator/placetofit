from django.contrib import admin
from .models import Job, Tag, JobType

class JobAdmin(admin.ModelAdmin):
	fieldsets = [
		('Not visible for users', {'fields': ['company']}),
		('General information', {'fields': ['title', 'city', 'job_type', 'salary']}),
		('Details', {'fields': ['description', 'requirements', 'responsibilities', 'bonuses']}),
		(None , {'fields': ['tags']}),
	]

admin.site.register(Job, JobAdmin)
admin.site.register(Tag)
admin.site.register(JobType)
