from django.contrib import admin
from .models import Company, City

admin.site.register(Company)
admin.site.register(City)
