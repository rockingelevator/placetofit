var gulp = require('gulp');
var jade = require('gulp-jade');
var uglify = require('gulp-uglify');
var ngTemplates = require('gulp-ng-templates');
//var watch = require('gulp-watch');
var stylus = require('gulp-stylus');
var connect = require('gulp-connect');
var concat = require('gulp-concat');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var svgSprite = require('gulp-svg-sprite');


//compile templates .jade to html
/*gulp.task('templates', function(){
    gulp.src('./jade/*.jade')
        .pipe(jade({
            pretty: true
        }))
        .pipe(gulp.dest('./templates/'))
        //.pipe(gulp.dest('./public/dist/templates/'))
});*/

//compile partials .jade to html
gulp.task('partials', function(){
   gulp.src('./templates/partials/*.jade')
       .pipe(jade({
           pretty: true
       }))
       .pipe(gulp.dest('./templates/partials/html/'))
});

gulp.task('ngtemplates', function(){
    return gulp.src('./templates/partials/html/*.html')
        .pipe(ngTemplates({
            filename: 'partials.js',
            module: 'app',
            path: function (path, base) {
                //return path.replace(base, '').replace('/partials', '');
                return path.replace(base, '/templates/partials/');
            }
        }))
        .pipe(uglify())
        //.pipe(rename({
        //    suffix: '.min'
        //}))
        .pipe(gulp.dest('./static/dist/templates'));
});

gulp.task('stylus', function(){
    gulp.src('./styl/*.styl')
        .pipe(stylus())
        .pipe(gulp.dest('./static/css/'))
        .pipe(concat('bundle.css'))
        .pipe(gulp.dest('./static/dist/css/'))
        .pipe(minifyCss({keepBreaks: true}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./static/dist/css/'));
});


gulp.task('connect', function(){
    connect.server();
});

gulp.task('svg-sprite', function(){
    gulp.src('./static/svg/*.svg')
    .pipe(svgSprite( 
            {
                mode                : {
                    css             : {     // Activate the «css» mode
                        render      : {
                            css     : true  // Activate CSS output (with default options)
                        }
                    },
                    symbol: true
                }
            }
        ))
    .pipe(gulp.dest('./static/svg/sprite/'));
});

gulp.task('default', ['partials', 'ngtemplates', 'stylus', 'svg-sprite']);
