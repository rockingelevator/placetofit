from django.conf.urls import patterns, url
from jobs.api import views

urlpatterns = patterns('',
	url(r'^$', views.JobListView.as_view(), name='job_list'),
	url(r'^tags/$', views.TagListView.as_view(), name='tag_list'),
	url(r'^job_types/$', views.JobTypeListView.as_view(), name='job_type_list'),
)