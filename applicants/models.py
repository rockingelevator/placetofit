from django.db import models
from django.contrib.auth.models import User

GENDER_CHOICES = (
	('male', 'Male'),
	('female', 'Female'),
)

ENGLISH_LEVEL_CHOICES = (
	('no', 'Don\'t speak'),
	('basic', 'Basic'),
	('intermediate', 'Intermediate'),
	('advanced', 'Advanced'),
	('native', 'Native'),
)

LINK_TYPE_CHOICES = (
	('github', 'GitHub'),
	('linkedin', 'LinkedIn'),
	('facebook', 'Facebook'),
	('twitter', 'Twitter'),
	('personal', 'Personal website'),
	('other', 'Other'),
)

class Applicant(models.Model):
	user = models.OneToOneField(User)
	gender = models.CharField(max_length=20, choices=GENDER_CHOICES, default='male')
	date_of_birth = models.DateField()
	city = models.CharField(max_length=100, blank=True, null=True)
	phone = models.CharField(max_length=20, blank=True, null=True)
	skype = models.CharField(max_length=50, blank=True, null=True)
	eng_level = models.CharField(max_length=20, choices=ENGLISH_LEVEL_CHOICES, default='intermediate')

	def __str__(self):
		return "%s %s" % (self.user.first_name, self.user.last_name)

class Education(models.Model):
	DEGREE_CHOICES = (
		('some', 'Somedegree'),
		('phd', 'Doctor'),
	)
	institute = models.CharField(max_length=150)
	field_of_study = models.CharField(max_length=150)
	degree = models.CharField(max_length=20, choices=DEGREE_CHOICES, default='phd')
	date_from = models.DateField(null=True, blank=True, verbose_name="Since")
	date_till = models.DateField(null=True, blank=True, verbose_name="Till")

	class Meta():
		verbose_name_plural = 'education'

	def __str__(self):
		return "%s, %s" % (self.institute, self.field_of_study)

class Skill(models.Model):
	title = models.CharField(max_length=50)
	rate = models.IntegerField(null=True, blank=True)

	def __str__(self):
		return self.title

class Experience(models.Model):
	company = models.CharField(max_length=100)
	city = models.CharField(max_length=150, null=True, blank=True)
	url = models.URLField(null=True, blank=True)
	position = models.CharField(max_length=150)
	date_from = models.DateField(blank=True, null=True, verbose_name="Start work")
	date_till = models.DateField(blank=True, null=True, verbose_name="Work till")

	class Meta():
		verbose_name_plural = 'experience'

	def __str__(self):
		return "%s at %s" % (self.position, self.company)

class Link(models.Model):
	link_type = models.CharField(max_length=20, choices=LINK_TYPE_CHOICES, default='other')
	#need to be Char instead URLField 'cause it's allowed to use just usernames for social links
	url = models.CharField(max_length=200) 

	def __str__(self):
		return "%s: %s" % (self.link_type, self.url)

class Resume(models.Model):
	applicant = models.ForeignKey(Applicant)
	short_description = models.CharField(max_length=140, blank=True, null=True)
	education = models.ManyToManyField(Education)
	skills = models.ManyToManyField(Skill)
	experience = models.ManyToManyField(Experience)
	links = models.ManyToManyField(Link)

	def __str__(self):
		return "%s %s" % (applicant.user.first_name, applicant.user.last_name)



	

