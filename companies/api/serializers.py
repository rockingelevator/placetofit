from django.forms import widgets
from rest_framework import serializers
from companies.models import City

class CitySerializer(serializers.ModelSerializer):
	class Meta:
		model = City
		fields = ('id', 'city', 'country')