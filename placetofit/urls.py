from django.conf.urls import patterns, include, url
from rest_framework.urlpatterns import format_suffix_patterns
from django.contrib import admin
from .views import dummy_render
from jobs import views as job_views

urlpatterns = patterns('',
    url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/jobs/', include('jobs.api.urls', namespace="api_jobs")),
    url(r'^api/companies/', include('companies.api.urls', namespace="api_companies")),
    url(r'^$', job_views.JobList.as_view()),
    url(r'^jobs/', include('jobs.urls', namespace="jobs")),
)

urlpatterns = format_suffix_patterns(urlpatterns)
