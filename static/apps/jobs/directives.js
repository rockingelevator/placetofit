angular.module('jobs.directives', ['duScroll', 'jobs.services'])

	.directive("fixInHeader", function ($window) {
	    return function(scope, element, attrs) {
	        var checkPosition = function(pos){
	        	if (pos >= 50) {
	                 element.addClass('fixed-logo');
	             } else {
	                 element.removeClass('fixed-logo');
	             }
	        };
	        angular.element($window).bind("scroll", function() {
	             checkPosition(this.pageYOffset)
	        });
	        //initial check
	        checkPosition($window.pageYOffset);
	    };
	})

	.directive("filtersTrigger", function(){
		return function(scope, element, attrs){
			var filters = angular.element(document.getElementById('filters'));
			element.bind('click', function(){
				if(filters.hasClass('opened')){
					filters.removeClass('opened');
				}
				else{
					filters.addClass('opened');
				}
			});
		};
	})

	.directive("searchTags", function(){
		return {
			scope: {
				collection: '='
			},
			link: function(scope, element, attr){
				scope.collection = [
					{
						'tag_type': 'Skills',
						'title': 'Python'
					},
					{
						'tag_type': 'Skills',
						'title': 'JavaScript'
					},
					{
						'tag_type': 'Categories',
						'title': 'Startup'
					},
					{
						'tag_type': 'Categories',
						'title': 'Fintech'
					}
				];
				//$("#search").find('input').on('input',function(){
				//	console.log('changed!');
				//});
				$(".search").children('.select2-search-field:input').val('fuck!');
			}
		};
	});

