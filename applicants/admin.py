from django.contrib import admin
from .models import Applicant, Resume, Education, Skill, Experience, Link

admin.site.register(Applicant)
admin.site.register(Education)
admin.site.register(Skill)
admin.site.register(Experience)
admin.site.register(Link)
admin.site.register(Resume)
