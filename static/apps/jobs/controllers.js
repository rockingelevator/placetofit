//**************************************************
//easing function
var linear = function (t) { return t };

//**************************************************
//module
angular.module('jobs.controllers', [
	'duScroll', 
	'ui.select', 
	'ngSanitize',
	'jobs.services'
	])
	.value('duScrollEasing', linear)
	.controller('JobsController', 
	function(
		$scope, 
		$document, 
		$window, 
		$location,
		$timeout,
		$http,
		CityService,
		CityList,
		TagService,
		FilterList,
		JobService
	){	

		var header = angular.element(document.getElementById('header'));
		var container = angular.element(document.getElementById('jobs'));
		var searchbar = angular.element(document.getElementById('searchbar'));
		var searchObj = document.getElementById('search');
		var teaser = angular.element(document.getElementById('teaser'));
		var containerObj = document.getElementById('jobs');
		var containerRect = containerObj.getBoundingClientRect();

		//***************************************************
		//snaps scroll to list and back to header, changing url path accordinally ***

		//if user enter direct link to jobs container
		if($location.path() == '/jobs/'){
			$document.scrollToElement(container, 80, 100);	
		}
		else{
			teaser.addClass('show');
		}
		
		//if search in focus scroll to it
		$('.select2-choices').on('click', function(){
			if(containerRect.top >= 81){
				$document.scrollToElement(container, 80, 100);
			}
		});

		//binding location, snap and scroll
		$document.on('scroll', function(){
			var pageY = $window.pageYOffset;
			var path = $location.path()
			if(containerObj.getBoundingClientRect().top < containerRect.top){
				//Scroll down
				$location.path('/jobs/');
				if(pageY >= 80 && pageY < 500){ 
					teaser.removeClass('show');
					$document.scrollToElement(container, 80, 100).then(function(){
					});
				}
			}
			else{
				//Scroll up
				if(pageY < 400 && path == '/jobs/'){
					$document.duScrollTop(0, 100).then(function(){
						$location.path('/');
					});
					teaser.addClass('show');
				}
			}
			containerRect = containerObj.getBoundingClientRect();
			
			//attaches search bar to header when scroll
			if(containerRect.top <= 85){
				searchbar.addClass('fixed-bar');
				header.addClass('header-filled');//makes header not transparent
			}
			else{
				searchbar.removeClass('fixed-bar');
				header.removeClass('header-filled');
			}
		});

		// ********************************************************
		$scope.search = {
			tags: [
				/*{
					'tag_type': 'Skills',
					'title': 'Python'
				},
				{
					'tag_type': 'Skills',
					'title': 'JavaScript'
				},
				{
					'tag_type': 'Categories',
					'title': 'Startup'
				},
				{
					'tag_type': 'Categories',
					'title': 'Fintech'
				},*/
			]
		};

		$scope.jobTypes = [
			{
				'id': 0,
				'title': 'All types'
			},
			{
				'id': 1,
				'title': 'Fulltime'
			},
			{
				'id': 2,
				'title': 'Parttime'
			},
		];

		$scope.salaries = [
			{
				'title': 'All salaries',
				'min_salary': '0',
			},
			{
				'title': 'About €1000',
				'min_salary': '0',
				'max_salary': '1500'
			},
			{
				'title': '€1500 - €2500',
				'min_salary': '1500',
				'max_salary': '2500'
			},
			{
				'title': '€2500 - €3500',
				'min_salary': '2500',
				'max_salary': '3500'
			},
			{
				'title': '€3500 and more',
				'min_salary': '3500',
			},
		];

		//filters default values
		$scope.cities = CityList.get();
		$scope.defaultJobType = $scope.jobTypes[0];
		$scope.defaultSalary = $scope.salaries[0];

		//dict of all search options
		$scope.options = {
			tags: [],
			city: $scope.cities[0],
			jobType: $scope.defaultJobType,
			salary: $scope.defaultSalary
		};

		//gets cities for the first time from server, then stores them in service
		$scope.getCities = function(){
			if($scope.cities.length < 2){
				$scope.loadingCities = true; //shows loader while loading cities
				CityService.get().$promise.then(function(data, headers){
						CityList.set(data);
						$scope.cities = CityList.get();
						$scope.loadingCities = false;
				}, function(err){
					$scope.loadingCities = false;
				});
			}
		};

		$scope.search.refreshTags = function(query){
			$scope.loadingTags = true;
			return TagService.get({'search': query}).$promise.then(function(data){
				$scope.search.tags = data;
				$scope.loadingTags = false;
			}, function(err){
				$scope.loadingTags = false;
			});
		};

		//setting default filters
		FilterList.setFilters($scope.options);
		$scope.$watch('options', function(n, o){
			if(FilterList.isChanged($scope.options)){ //checking if filters changed. fix initial trigger
				//preparing get peremeters
				var city = $scope.options.city.id ? $scope.options.city.id : "";
				var job_type = $scope.options.jobType.id != 0 ? $scope.options.jobType.title : "";
				var max_salary = $scope.options.salary.max_salary ? $scope.options.salary.max_salary : "";
				var params = {
					'city': city,
					'job_type': job_type,
					'min_salary': $scope.options.salary.min_salary,
					'max_salary': max_salary,
					'tags': $scope.options.tags
				};
				$scope.loadingJobs = true;
				JobService.get(params).$promise.then(function(data){
					$('#first_list').remove();
					$scope.jobs = data;
					$scope.loadingJobs = false;
				}, function(err){
					$scope.loadingJobs = false;
				});
			}
		}, true);
	});