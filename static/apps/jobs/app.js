angular.module('jobs', [
	'ngRoute',
	'ngResource',
	'ui.select',
	'ngSanitize',
	'jobs.services',
	'jobs.controllers',
	'jobs.directives'
	])

	.config(function($interpolateProvider) {
  		$interpolateProvider.startSymbol('{$');
		$interpolateProvider.endSymbol('$}');
	})

	.config(['$locationProvider', function($locationProvider) {
    	$locationProvider.html5Mode(true);
  	}])

  	.config(function(uiSelectConfig) {
		uiSelectConfig.theme = 'select2';
		uiSelectConfig.resetSearchInput = true;
	})
	
	.config(function($resourceProvider) {
	  $resourceProvider.defaults.stripTrailingSlashes = false;
	})

	//for angular-ui-select filter
	.filter('propsFilter', function() {
	  return function(items, props) {
	    var out = [];

	    if (angular.isArray(items)) {
	      items.forEach(function(item) {
	        var itemMatches = false;

	        var keys = Object.keys(props);
	        for (var i = 0; i < keys.length; i++) {
	          var prop = keys[i];
	          var text = props[prop].toLowerCase();
	          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
	            itemMatches = true;
	            break;
	          }
	        }

	        if (itemMatches) {
	          out.push(item);
	        }
	      });
	    } else {
	      // Let the output be the input untouched
	      out = items;
	    }

	    return out;
	  };
	})

	.filter('title', function () {
		return function (input) {
			var words = input.split(' ');
			for (var i = 0; i < words.length; i++) {
				words[i] = words[i].toLowerCase(); // lowercase everything to get rid of weird casing issues
				words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1);
			}
			return words.join(' ');
		}
	});
/*
  	.config(['$routeProvider', function($routeProvider, $httpProvider) {
		$routeProvider

		.when('/', {
			controller: 'LandingController',
			template: ' '
		});

		//.otherwise({
		//	redirectTo: '/'
		//});
	}]);*/