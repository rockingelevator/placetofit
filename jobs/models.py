from django.db import models
from companies.models import Company, City
from applicants.models import Resume

class JobType(models.Model):
	job_type = models.CharField(max_length=25)

	def __str__(self):
		return self.job_type

class Tag(models.Model):
	"""
	Predifined tags. Tags are used for job search. 
	"""
	TAG_TYPE_CHOICES = (
		('skill', 'Skill'),
		('category', 'Category'),
	)
	tag_type = models.CharField(max_length=20, choices=TAG_TYPE_CHOICES, default='skill')
	title = models.CharField(max_length=50)

	def __str__(self):
		return "%s, %s" % (self.title, self.tag_type)
		


class Job(models.Model):
	company = models.ForeignKey(Company)
	title = models.CharField(max_length=50, null=True)
	city = models.ForeignKey(City)
	job_type = models.ForeignKey(JobType)
	salary = models.IntegerField()
	description = models.TextField(null=True, blank=True)
	requirements = models.TextField(null=True, blank=True)
	responsibilities = models.TextField(null=True, blank=True)
	description = models.TextField(null=True, blank=True)
	bonuses = models.TextField(null=True, blank=True)
	tags = models.ManyToManyField(Tag)
	pub_date = models.DateTimeField(auto_now=True)
	applicants = models.ManyToManyField(Resume, null=True, blank=True)

	class Meta():
		ordering = ['-pub_date']

	def __str__(self):
		return "%s at %s, %s" % (self.title, self.company.name, self.city)

