from django.shortcuts import render
from django.views.generic import ListView, DetailView
from .models import Job

class JobList(ListView):
	model = Job
	context_object_name = "jobs"
	template_name = "jobs.jade"

class JobDetailView(DetailView):
	context_object_name = 'job'
	template_name = "job_detail.jade"
	queryset = Job.objects.all()
