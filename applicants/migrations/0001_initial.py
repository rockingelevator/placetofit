# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Applicant',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('gender', models.CharField(choices=[('male', 'Male'), ('female', 'Female')], default='male', max_length=20)),
                ('date_of_birth', models.DateField()),
                ('city', models.CharField(null=True, max_length=100, blank=True)),
                ('phone', models.CharField(null=True, max_length=20, blank=True)),
                ('skype', models.CharField(null=True, max_length=50, blank=True)),
                ('eng_level', models.CharField(choices=[('no', "Don't speak"), ('basic', 'Basic'), ('intermediate', 'Intermediate'), ('advanced', 'Advanced'), ('native', 'Native')], default='intermediate', max_length=20)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Education',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('institute', models.CharField(max_length=150)),
                ('field_of_study', models.CharField(max_length=150)),
                ('degree', models.CharField(choices=[('some', 'Somedegree'), ('phd', 'Doctor')], default='phd', max_length=20)),
                ('date_from', models.DateField(null=True, verbose_name='Since', blank=True)),
                ('date_till', models.DateField(null=True, verbose_name='Till', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Experience',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('company', models.CharField(max_length=100)),
                ('city', models.CharField(null=True, max_length=150, blank=True)),
                ('url', models.URLField(null=True, blank=True)),
                ('position', models.CharField(max_length=150)),
                ('date_from', models.DateField(null=True, verbose_name='Start work', blank=True)),
                ('date_till', models.DateField(null=True, verbose_name='Work till', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Link',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('link_type', models.CharField(choices=[('github', 'GitHub'), ('linkedin', 'LinkedIn'), ('facebook', 'Facebook'), ('twitter', 'Twitter'), ('personal', 'Personal website'), ('other', 'Other')], default='other', max_length=20)),
                ('url', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Resume',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('short_description', models.CharField(null=True, max_length=140, blank=True)),
                ('applicant', models.ForeignKey(to='applicants.Applicant')),
                ('education', models.ManyToManyField(to='applicants.Education')),
                ('experience', models.ManyToManyField(to='applicants.Experience')),
                ('links', models.ManyToManyField(to='applicants.Link')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Skill',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('rate', models.IntegerField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='resume',
            name='skills',
            field=models.ManyToManyField(to='applicants.Skill'),
            preserve_default=True,
        ),
    ]
