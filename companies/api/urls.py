from django.conf.urls import patterns, url
from companies.api import views 

urlpatterns = patterns('',
	url(r'^cities/$', views.CityListView.as_view(), name='city_list'),
	url(r'^cities/(?P<pk>[0-9]+)/$', views.CityView.as_view(), name='city_detail'),
)