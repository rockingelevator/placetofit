from django.forms import widgets
from rest_framework import serializers
from companies.api.serializers import CitySerializer
from jobs.models import Job, Tag, JobType

class JobListSerializer(serializers.ModelSerializer):
	city = CitySerializer(read_only=True)
	job_type = serializers.ReadOnlyField(source='job_type.title')
	class Meta:
		model = Job
		fields = ('id', 'title', 'city', 'job_type', 'salary')

class TagListSerializer(serializers.ModelSerializer):
	class Meta:
		model = Tag

class JobTypeListSerializer(serializers.ModelSerializer):
	class Meta:
		model = JobType
