# README #

## Setting-up ##

###First of all###
It's all about Python 3. Go and get it right now: [python3](https://www.python.org/downloads/)

###Virtualenv###
Make sure you've created and are currently working on a new virtual environment
Feel free to roam this tutorial anyway: [Virtualenv Tutorial](http://simononsoftware.com/virtualenv-tutorial-part-2/)
Don't forget to add path to your python3 while creating new environment:
```
#!shell

$ which python3
/path/to/your/python3
$ mkvirtualenv placetofit -p /path/to/your/python3
```

###Clone###

```
#!shell

$ git clone https://rockingelevator@bitbucket.org/rockingelevator/placetofit.git
$ cd placetofit
```
*- Captain Obvious with love*

###Install requirements###
Install required python libraries:
```
#!shell

$ pip install -r requirements.txt
```

###Gulp###
If you want just to run project you can skip this step. Otherwise you'll need gulp for managing tasks:
```
#!shell

$ npm install --global gulp
```

Install modules:
```
#!shell

$ npm install 
```

###Bower Components###
Bower is required:
```
#!shell

$ npm install -g bower
```

Install required components:
```
#!shell

$ bower install
```

###Run###

```
#!shell

$python manage.py runserver --settings=placetofit.settings.dev
```
Visit [http://127.0.0.1:8000](http://127.0.0.1:8000)

###Django Admin###
[http://127.0.0.1:8000/admin/](http://127.0.0.1:8000/admin/)
User: *admin* 
Pwd: *admin*

###***###
![4ae570f93caa8fc03886002714a8b3f1.gif](https://bitbucket.org/repo/6jBdLL/images/1216110086-4ae570f93caa8fc03886002714a8b3f1.gif)