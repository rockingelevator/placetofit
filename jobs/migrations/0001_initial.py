# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('applicants', '0001_initial'),
        ('companies', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('title', models.CharField(null=True, max_length=50)),
                ('salary', models.IntegerField()),
                ('requirements', models.TextField(null=True, blank=True)),
                ('responsibilities', models.TextField(null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('bonuses', models.TextField(null=True, blank=True)),
                ('pub_date', models.DateTimeField(auto_now=True)),
                ('applicants', models.ManyToManyField(to='applicants.Resume')),
                ('city', models.ForeignKey(to='companies.City')),
                ('company', models.ForeignKey(to='companies.Company')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='JobType',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('job_type', models.CharField(max_length=25)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='job',
            name='job_type',
            field=models.ForeignKey(to='jobs.JobType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='job',
            name='tags',
            field=models.ManyToManyField(to='jobs.Tag'),
            preserve_default=True,
        ),
    ]
