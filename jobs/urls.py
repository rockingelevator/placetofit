from django.conf.urls import patterns, include, url
from jobs import views as jobs_views

urlpatterns = patterns('',
    url(r'^$', jobs_views.JobList.as_view(), name="job_list"),
    url(r'^(?P<pk>[0-9]+)/$', jobs_views.JobDetailView.as_view(), name="job_detail"),
)