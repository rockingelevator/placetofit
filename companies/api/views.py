from companies.models import City
from rest_framework.response import Response
from rest_framework import generics
from .serializers import CitySerializer

class CityView(generics.RetrieveAPIView):
	"""
	City Details
	"""
	queryset = City.objects.all()
	serializer_class = CitySerializer

class CityListView(generics.ListAPIView):
	"""
	List of cities where service works
	"""
	queryset = City.objects.all()
	serializer_class = CitySerializer