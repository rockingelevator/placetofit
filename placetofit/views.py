from django.shortcuts import render

def dummy_render(request, template):
	return render(request, template)