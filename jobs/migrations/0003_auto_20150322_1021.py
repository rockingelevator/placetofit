# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0002_auto_20150315_2028'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='job',
            options={'ordering': ['-pub_date']},
        ),
        migrations.AddField(
            model_name='tag',
            name='tag_type',
            field=models.CharField(max_length=20, choices=[('skill', 'Skill'), ('category', 'Category')], default='skill'),
            preserve_default=True,
        ),
    ]
